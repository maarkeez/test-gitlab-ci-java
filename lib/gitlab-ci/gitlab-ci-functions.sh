#!/bin/bash

function build_project {
	set -e;
	echo "Building maven project";
	mvn clean install -DskipTests;
}

function unitary_testing {
	set -e;
	echo "Running all tests";
    mvn clean verify;
}

function build_and_deploy {
	set -e;
	echo "Deployed!"
}

function create_tag {
	set -e;
	
	git_set_up;

	echo "creating tag"
	git tag -a 1.0.0 -m "Version 1.0.0";

	echo "push tag"
	git push origin --tags;
}

function update_version {
	NEW_VERSION=$1
	
	set -e;
	git_set_up;
	
	echo "git checkout $CI_COMMIT_REF_NAME"
	git checkout $CI_COMMIT_REF_NAME

	echo "Updating version to: $NEW_VERSION"
	mvn org.codehaus.mojo:versions-maven-plugin:2.7:set -DnewVersion=$NEW_VERSION -DgenerateBackupPoms=false

	git add .
	git commit -a -m "Updated version with $NEW_VERSION"
	git push

}

function git_set_up {

	echo "Removing git remote url"
	git remote remove origin

	echo "Adding git remote url"
	git remote add origin https://${GIT_USER}:${GIT_TOKEN}@gitlab.com/${GIT_USER}/${CI_PROJECT_NAME}.git
	
	echo "Adding username and email to git"
	git config user.email ${GIT_EMAIL}
	git config user.name ${GIT_USER}

	git fetch

}